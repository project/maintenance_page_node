# Maintenance Page Node

This module allows you to choose a node to show when maintenance mode is on.
When the module is installed, navigate to maintenance mode form
(`admin/config/development/maintenance`) and an autocomplete node field is
added. When this field is empty, the default message will be shown.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/maintenance_page_node).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/maintenance_page_node).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module does not have any dependency on any other module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

You can configure from `admin/config/development/maintenance`


## Maintainers

- Aaron Sanchez - [asanchezs](https://www.drupal.org/u/asanchezs)
