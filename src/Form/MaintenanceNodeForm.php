<?php

namespace Drupal\maintenance_page_node\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\system\Form\SiteMaintenanceModeForm;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Maintenance Node settings for this site.
 */
class MaintenanceNodeForm extends SiteMaintenanceModeForm {

  /**
   * The entity type manager.
 *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
 */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new SiteMaintenanceModeForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state keyvalue collection to use.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, StateInterface $state, PermissionHandlerInterface $permission_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory, $typedConfigManager, $state, $permission_handler);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SiteMaintenanceModeForm|MaintenanceNodeForm|ConfigFormBase|static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('state'),
      $container->get('user.permissions'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'maintenance.node',
      'system.maintenance',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $maintenance_node_id = $this->config('maintenance.node')->get('maintenance_node');
    $maintenance_node = $maintenance_node_id ?
      $this->entityTypeManager->getStorage('node')->load($maintenance_node_id) : '';

    $form['maintenance_node'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Maintenance Node'),
      '#description' => $this->t('Node to show when maintenance mode is on. When this field is empty, the default message will be shown.'),
      '#default_value' => $maintenance_node,
      '#weight' => 99,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('maintenance.node')
      ->set('maintenance_node', $form_state->getValue('maintenance_node'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
